var bulletTime = 0;
var scoreText;
var LiveText;
var LevelText;
var Invincible = false;
var Lazering = false
var skillEnergy = 0
var monsterDead = 0
var Bevent
var BossShooted = false
var PlayState = {
    preload: function () {

    },
    create: function () {

        /* game.state.start('over') */
        this.music = game.add.audio('bgMusic', game.global.musicVolume, true)
        this.music.play()
        this.Mdie = game.add.audio('Mdie', game.global.musicVolume, false)
        game.world.setBounds(0, 0, 850, 1920);
        game.add.sprite(0, 0, 'background');
        game.global.score = 0;

        scoreText = game.add.text(10, 10, 'Score : ' + game.global.score, {
            font: '34px Arial',
            fill: '#fff'
        });
        LiveText = game.add.text(10, 50, 'Lives : ', {
            font: '34px Arial',
            fill: '#fff'
        });
        LevelText = game.add.text(game.world.width / 2, 10, 'Level : ' + game.global.level, {
            font: '34px Arial',
            fill: '#fff'
        });
        LevelText.anchor.setTo(0.5, 0)
        scoreText.fixedToCamera = true;
        LiveText.fixedToCamera = true;
        LevelText.fixedToCamera = true;

        this.hearts = game.add.group();
        game.add.sprite(240, 60, 'heart', 0, this.hearts)
        game.add.sprite(200, 60, 'heart', 0, this.hearts)
        game.add.sprite(160, 60, 'heart', 0, this.hearts)
        game.add.sprite(120, 60, 'heart', 0, this.hearts)
        this.hearts.setAll('fixedToCamera', 'true')

        game.physics.startSystem(Phaser.Physics.ARCADE);

        this.item1 = game.add.sprite(game.world.width / 2 - 80, 1500, 'item1')
        this.item2 = game.add.sprite(game.world.width / 2, 1500, 'item2')
        this.item3 = game.add.sprite(game.world.width / 2 + 80, 1500, 'item3')
        this.item1.anchor.setTo(0.5, 0.5)
        this.item2.anchor.setTo(0.5, 0.5)
        this.item3.anchor.setTo(0.5, 0.5)
        this.item1.name = 'item1'
        this.item2.name = 'item2'
        this.item3.name = 'item3'
        game.physics.arcade.enable(this.item1)
        game.physics.arcade.enable(this.item2)
        game.physics.arcade.enable(this.item3)

        this.player = game.add.sprite(game.world.width / 2,
            game.world.height - 120, 'player');
        this.player.anchor.setTo(0.5, 0);
        this.player.animations.add('leftwalk', [1], 1, true)
        this.player.animations.add('rightwalk', [2], 1, true)

        if (game.global.playerNum == 'two') {
            this.playerTwo = game.add.sprite(game.world.width / 2,
                game.world.height - 120, 'p2');
            this.playerTwo.anchor.setTo(0.5, 0);
            this.shield = game.add.sprite(game.world.width / 2,
                game.world.height - 120, 'shield');
            this.shield.anchor.setTo(0.5, 0);
        }


        this.setBullet()

        game.time.events.loop(120, this.fireBullet, this);
        game.time.events.loop(400, () => {
            if (!this.item1.alive && this.expbullet) {
                var bullet = this.expbullet.getFirstExists(false)
                if (bullet) {
                    bullet.reset(this.player.x, this.player.y - 8);
                    bullet.body.velocity.y = -450;
                    if (!this.item2.alive) {
                        if (this.monsterTwo.countLiving() != 0) {
                            var mon = this.monsterTwo.getFirstAlive()
                            var t = (mon.y - this.player.y) / -450
                            bullet.body.velocity.x = (mon.x - this.player.x) / t / 2
                        } else if (this.GameBoss.alive) {
                            var t = (this.GameBoss.y - this.player.y) / -450
                            bullet.body.velocity.x = (this.GameBoss.x - this.player.x) / t / 2
                        }
                    }

                }
            }
        })
        game.physics.arcade.enable(this.player);
        if (this.playerTwo) game.physics.arcade.enable(this.playerTwo);


        this.cursor = game.input.keyboard.createCursorKeys();

        game.camera.y = game.world.height;
        this.player.body.collideWorldBounds = true;
        if (this.playerTwo) this.playerTwo.body.collideWorldBounds = true;

        this.emitter = game.add.emitter(422, 320, 15);
        this.emitter.makeParticles('pixel');
        this.emitter.setYSpeed(-150, 150);
        this.emitter.setXSpeed(-150, 150);
        this.emitter.setScale(1, 0, 1, 0, 800);
        this.emitter.gravity = 100;

        this.emitter2 = game.add.emitter(422, 320, 15);
        this.emitter2.makeParticles('pixel');
        this.emitter2.setYSpeed(-150, 150);
        this.emitter2.setXSpeed(-150, 150);
        this.emitter2.setScale(1, 0, 1, 0, 800);
        this.emitter2.gravity = 100;

        this.emitter3 = game.add.emitter(422, 320, 15);
        this.emitter3.makeParticles('pixel');
        this.emitter3.setYSpeed(-150, 150);
        this.emitter3.setXSpeed(-150, 150);
        this.emitter3.setScale(1, 0, 1, 0, 800);
        this.emitter3.gravity = 100;

        this.expEmitter = game.add.emitter(0, 0, 1600)
        this.expEmitter.makeParticles('blood');
        this.expEmitter.setYSpeed(-350, 350);
        this.expEmitter.setXSpeed(-350, 350);
        this.expEmitter.setScale(1, 0, 1, 0, 800);
        this.expEmitter.gravity = 0;

        this.pauseButton = game.add.sprite(game.world.width - 40, 40, 'pause')
        this.pauseButton.anchor.setTo(0.5, 0.5)
        this.pauseButton.fixedToCamera = true
        this.pauseButton.inputEnabled = true;
        this.pauseButton.events.onInputUp.add(function () {
            if (!game.paused) {
                this.resumeButton = game.add.sprite(game.world.width / 2 - 135, 250, 'resume')
                this.restartButton = game.add.sprite(game.world.width / 2 - 135, 330, 'restart')
                this.quittButton = game.add.sprite(game.world.width / 2 - 135, 410, 'quit')
                this.soundBar = game.add.sprite(game.world.width / 2 - 135, 185, 'soundBar')
                this.soundPB = game.add.sprite(game.world.width / 2 + 70, 185, 'soundPlus')
                this.soundMB = game.add.sprite(game.world.width / 2 - 60, 185, 'soundMinus')
                this.volumeText = game.add.text(game.world.width / 2 + 10, 195, parseFloat(game.global.musicVolume).toFixed(1), {
                    font: '30px Arial',
                    fill: '#fff'
                })

                this.resumeButton.fixedToCamera = true
                this.restartButton.fixedToCamera = true
                this.quittButton.fixedToCamera = true
                this.soundBar.fixedToCamera = true
                this.soundPB.fixedToCamera = true
                this.soundMB.fixedToCamera = true
                this.volumeText.fixedToCamera = true

                game.time.events.add(Phaser.Timer.SECOND * 0.05, () => {
                    game.paused = !game.paused
                }, this);
            }

        });
        game.input.onDown.add(this.pauseMenu, self);


        this.energyBar = game.add.sprite(10, 100, 'energyBar')
        this.energyBar.fixedToCamera = true
        this.energyBarOn = game.add.sprite(10, 100, 'barOn')
        this.energyBarOn.fixedToCamera = true

        this.spaceKey = game.input.keyboard.addKey(Phaser.Keyboard.SPACEBAR)
        this.Wkey = game.input.keyboard.addKey(Phaser.Keyboard.W)
        this.Akey = game.input.keyboard.addKey(Phaser.Keyboard.A)
        this.Skey = game.input.keyboard.addKey(Phaser.Keyboard.S)
        this.Dkey = game.input.keyboard.addKey(Phaser.Keyboard.D)




        this.setMonster()
        game.time.events.loop(3001, () => {

            if (this.bossAttack) {
                this.GameBoss.loadTexture('bossAttack', 0);
                this.GameBoss.animations.add('attacking')
                this.Aevent = game.time.events.loop(500, () => {
                    this.GameBoss.play('attacking', 30, true)
                    BossShooted = false
                })
            }
            /* else {
                           this.GameBoss.loadTexture('bossNormal', 0);
                           this.GameBoss.animations.add('normaling')
                           Aevent = game.time.events.loop(500, () => {
                               this.GameBoss.play('normaling', 30, true)
                           })
                       }
                       this.bossAttack = !this.bossAttack */
        }, this)
        /* game.state.start('over') */
        this.explode = game.add.sprite(game.world.width / 2,
            -50, 'explode');
        this.explode.anchor.setTo(0.5, 0.5)
        this.explode.animations.add('explode')
        if (this.playerTwo) {
            this.blade = game.add.sprite(this.playerTwo.x, this.playerTwo.y, 'blade')
            this.blade.animations.add('blading')
            this.blade.anchor.setTo(0.5, 1)
            this.blade.animations.play('blading', 100, true)
            this.blade.name = 'blade'
            game.physics.arcade.enable(this.shield)
            game.physics.arcade.enable(this.blade)

        }
    },
    update: function () {

        game.camera.y -= 2;
        if (game.camera.y != 0) {
            this.player.y -= 2;
            if (this.playerTwo) this.playerTwo.y -= 2
        }

        this.PlayerMove();

        scoreText.text = 'Score : ' + game.global.score

        game.physics.arcade.overlap(this.player, this.item1, this.getItem, null, this)
        game.physics.arcade.overlap(this.player, this.item2, this.getItem, null, this)
        game.physics.arcade.overlap(this.player, this.item3, this.getItem, null, this)
        game.physics.arcade.overlap(this.player, this.monsterOne, this.playerHit, null, this)
        game.physics.arcade.overlap(this.player, this.monsterTwo, this.playerHit, null, this)
        game.physics.arcade.overlap(this.player, this.M2Bullets, this.playerHit, null, this)
        game.physics.arcade.overlap(this.player, this.GameBoss, this.playerHit, null, this)

        if (this.GameBoss) game.physics.arcade.overlap(this.player, this.GameBoss, this.playerHit, null, this)
        if (this.GameBoss) game.physics.arcade.overlap(this.player, this.BossBul, this.playerHit, null, this)
        if (this.GameBoss) game.physics.arcade.overlap(this.player, this.BossBulR, this.playerHit, null, this)

        if (this.expbullet) game.physics.arcade.overlap(this.monsterOne, this.expbullet, this.explosion, null, this)
        if (this.expbullet) game.physics.arcade.overlap(this.monsterTwo, this.expbullet, this.explosion, null, this)
        if (this.expbullet) game.physics.arcade.overlap(this.GameBoss, this.expbullet, this.explosion, null, this)

        if (this.helperBullet) game.physics.arcade.overlap(this.helperBullet, this.monsterOne, this.enemyHit, null, this)
        if (this.helperBullet) game.physics.arcade.overlap(this.helperBullet, this.monsterTwo, this.enemyHit, null, this)
        if (this.helperBullet) game.physics.arcade.overlap(this.helperBullet, this.GameBoss, this.enemyHit, null, this)

        if (this.shield) game.physics.arcade.overlap(this.shield, this.M2Bullets, this.defend, null, this)
        if (this.shield) game.physics.arcade.overlap(this.shield, this.BossBul, this.defend, null, this)
        if (this.shield) game.physics.arcade.overlap(this.shield, this.BossBulR, this.defend, null, this)


        if (this.monsterOne) {
            this.monsterOne.children.forEach(function (M1) {
                M1.angle += 10
            })
        }

        if (this.monsterTwo) {
            this.monsterTwo.children.forEach(function (M2) {
                M2.animations.play('move')
            })
        }

        if (this.GameBoss) {
            game.physics.arcade.overlap(this.bullets, this.GameBoss, this.enemyHit, null, this)
            game.physics.arcade.overlap(this.bulletsLeft, this.GameBoss, this.enemyHit, null, this)
            game.physics.arcade.overlap(this.bulletsRight, this.GameBoss, this.enemyHit, null, this)
        }
        game.physics.arcade.overlap(this.bullets, this.monsterOne, this.enemyHit, null, this)
        game.physics.arcade.overlap(this.bulletsLeft, this.monsterOne, this.enemyHit, null, this)
        game.physics.arcade.overlap(this.bulletsRight, this.monsterOne, this.enemyHit, null, this)
        game.physics.arcade.overlap(this.bullets, this.monsterTwo, this.enemyHit, null, this)
        game.physics.arcade.overlap(this.bulletsLeft, this.monsterTwo, this.enemyHit, null, this)
        game.physics.arcade.overlap(this.bulletsRight, this.monsterTwo, this.enemyHit, null, this)

        this.moveMonster()
        this.bulletKill()
        this.LazerSkill()
        if (this.music.volume != game.global.musicVolume) this.music.volume = game.global.musicVolume
        if (this.Mdie.volume != game.global.musicVolume) this.Mdie.volume = game.global.musicVolume
        if (this.GameBoss)
            this.GameBoss.animations.play('bossMove')
        
        if (game.camera.y == 0 && this.GameBoss && (!this.GameBoss.alive && game.global.level > 1) || game.global.level == 1 && this.monsterTwo.total == 0) {
            console.log("NEXT")
            game.camera.y = game.world.height;
            this.player.y = game.world.height - (600 - this.player.y)
            if(this.playerTwo)
            this.playerTwo.y = game.world.height - (600 - this.playerTwo.y)

            game.global.level++
            LevelText.text = 'Level : ' + game.global.level
            if (this.GameBoss) this.GameBoss.destroy()
            if (this.monsterOne) this.monsterOne.destroy()
            if (this.monsterTwo) this.monsterTwo.destroy()
            this.setMonster()
            this.hearts.destroy()
            this.hearts = game.add.group();
            game.add.sprite(240, 60, 'heart', 0, this.hearts)
            game.add.sprite(200, 60, 'heart', 0, this.hearts)
            game.add.sprite(160, 60, 'heart', 0, this.hearts)
            game.add.sprite(120, 60, 'heart', 0, this.hearts)
            this.hearts.setAll('fixedToCamera', 'true')
            game.time.events.remove(this.Aevent);
            this.Aevent = game.time.events.loop((500 - game.global.level * 10 < 100) ? 100 : 500 - game.global.level * 10, () => {
                this.GameBoss.play('attacking', 30, true)
                BossShooted = false
            })

        }

        if (this.GameBoss) {
            if (this.GameBoss.frame == 2 && this.bossAttack && !BossShooted) {
                var bb = this.BossBul.getFirstExists(false);
                if (bb) {
                    bb.reset(this.GameBoss.x - 130, this.GameBoss.y + 100);
                    bb.body.velocity.y = 450;
                    var tmp = (this.player.y - (this.GameBoss.y + 100)) / bb.body.velocity.y
                    bb.body.velocity.x = (this.player.x - (this.GameBoss.x - 130)) / tmp
                    if (bb.body.velocity.x > 450) bb.body.velocity.x = 450
                    if (bb.body.velocity.x < -450) bb.body.velocity.x = -450
                }
                var bbR = this.BossBulR.getFirstExists(false);
                if (bbR) {
                    bbR.reset(this.GameBoss.x + 130, this.GameBoss.y + 100);
                    bbR.body.velocity.y = 450;
                    var tmp = (this.player.y - (this.GameBoss.y + 100)) / bbR.body.velocity.y
                    bbR.body.velocity.x = (this.player.x - (this.GameBoss.x + 130)) / tmp
                    if (bbR.body.velocity.x > 450) bbR.body.velocity.x = 450
                    if (bbR.body.velocity.x < -450) bbR.body.velocity.x = -450
                }
                BossShooted = true
            }
        }
        /* if (this.GameBoss)
            console.log("Y:", this.GameBoss.y) */
        /* console.log(this.item1.x, this.item1.y, this.player.x, this.player.y)
         */
        if (this.helper) {
            this.helper.x = this.player.x + 100
            this.helper.y = this.player.y + 30
        }
        if (this.playerTwo) {
            this.shield.x = this.playerTwo.x
            this.shield.y = this.playerTwo.y - 20
            this.blade.x = this.playerTwo.x
            this.blade.y = this.playerTwo.y
            game.physics.arcade.overlap(this.blade, this.monsterTwo, this.enemyHit, null, this)
            if (skillEnergy >= 10)
                this.shield.visible = true
            else
                this.shield.visible = false
        }

    },
    render: function () {
        game.debug.cameraInfo(game.camera, 12, 162);

    },
    PlayerMove: function () {
        if (this.cursor.left.isDown) {
            this.player.x -= 5;
            this.player.animations.play('leftwalk');
        } else if (this.cursor.right.isDown) {
            this.player.x += 5;
            this.player.animations.play('rightwalk');
        } else {
            this.player.animations.stop();
            this.player.frame = 0;
        }
        if (this.cursor.up.isDown && this.player.y >= game.camera.y) {
            this.player.y -= 5;
        } else if (this.cursor.down.isDown && this.player.y <= game.camera.y + 530) {
            this.player.y += 5;
        }

        if (this.playerTwo) {
            if (this.Akey.isDown) {
                this.playerTwo.x -= 5;
            } else if (this.Dkey.isDown) {
                this.playerTwo.x += 5;
            }
            if (this.Wkey.isDown && this.playerTwo.y >= game.camera.y) {
                this.playerTwo.y -= 5;
            } else if (this.Skey.isDown && this.playerTwo.y <= game.camera.y + 530) {
                this.playerTwo.y += 5;
            }
        }

    },
    fireBullet: function () {

        var bullet = this.bullets.getFirstExists(false);
        if (bullet) {
            bullet.reset(this.player.x, this.player.y - 8);
            bullet.body.velocity.y = -650;
        }

        var bullet2 = this.bulletsLeft.getFirstExists(false);
        if (bullet2) {
            bullet2.reset(this.player.x, this.player.y - 8);
            bullet2.body.velocity.x = -200;
            bullet2.body.velocity.y = -650;
        }

        var bullet3 = this.bulletsRight.getFirstExists(false);
        if (bullet3) {
            bullet3.reset(this.player.x, this.player.y - 8);
            bullet3.body.velocity.x = 200;
            bullet3.body.velocity.y = -650;
        }

        if (this.helper) {
            var hb = this.helperBullet.getFirstExists(false);
            if (hb) {
                hb.reset(this.helper.x, this.helper.y - 8);
                hb.body.velocity.y = -650;
            }
        }


    },
    setBullet: function () {
        this.bullets = game.add.group()
        this.bullets.enableBody = true;
        this.bullets.createMultiple(200, 'bullet');
        this.bullets.setAll('anchor.x', 0.5);
        this.bullets.setAll('anchor.y', 0);
        this.bullets.setAll('outOfBoundsKill', true);
        this.bullets.setAll('checkWorldBounds', true);
        this.bullets.setAll('name', "b1");

        this.bulletsLeft = game.add.group()
        this.bulletsLeft.enableBody = true;
        this.bulletsLeft.createMultiple(200, 'bullet');
        this.bulletsLeft.setAll('anchor.x', 0.5);
        this.bulletsLeft.setAll('anchor.y', 0);
        this.bulletsLeft.setAll('outOfBoundsKill', true);
        this.bulletsLeft.setAll('checkWorldBounds', true);
        this.bulletsLeft.setAll('angle', -22)
        this.bulletsLeft.setAll('name', "b2");

        this.bulletsRight = game.add.group()
        this.bulletsRight.enableBody = true;
        this.bulletsRight.createMultiple(200, 'bullet');
        this.bulletsRight.setAll('anchor.x', 0.5);
        this.bulletsRight.setAll('anchor.y', 0);
        this.bulletsRight.setAll('outOfBoundsKill', true);
        this.bulletsRight.setAll('checkWorldBounds', true);
        this.bulletsRight.setAll('angle', 22)
        this.bulletsRight.setAll('name', 'b3')

        this.M2Bullets = game.add.group()
        this.M2Bullets.enableBody = true;
        this.M2Bullets.createMultiple(200, 'M2_bullet');
        this.M2Bullets.setAll('anchor.x', 0.5);
        this.M2Bullets.setAll('anchor.y', 0);
        this.M2Bullets.setAll('outOfBoundsKill', true);
        this.M2Bullets.setAll('checkWorldBounds', true);
        this.M2Bullets.setAll('name', "M2B");


        this.expbullet = game.add.group()
        this.expbullet.enableBody = true;
        this.expbullet.createMultiple(200, 'expbullet');
        this.expbullet.setAll('anchor.x', 0.5);
        this.expbullet.setAll('anchor.y', 0);
        this.expbullet.setAll('outOfBoundsKill', true);
        this.expbullet.setAll('checkWorldBounds', true);
        this.expbullet.setAll('name', "expbullet");

        this.helperBullet = game.add.group()
        this.helperBullet.enableBody = true;
        this.helperBullet.createMultiple(200, 'helpB');
        this.helperBullet.setAll('anchor.x', 0.5);
        this.helperBullet.setAll('anchor.y', 0);
        this.helperBullet.setAll('outOfBoundsKill', true);
        this.helperBullet.setAll('checkWorldBounds', true);
        this.helperBullet.setAll('name', "hb");

    },
    setMonster: function () {
        this.monsterOne = game.add.group()
        for (var i = 0; i < game.global.level; i++) {
            game.add.sprite(game.world.width / 2 + Math.floor(Math.random() * (200 - (-200) + 1)) + (-200), 100, 'MonsterOne', 0, this.monsterOne)
        }

        this.monsterOne.enableBody = true;
        this.monsterOne.physicsBodyType = Phaser.Physics.ARCADE;
        this.monsterOne.children.forEach(function (M1) {
            game.physics.arcade.enable(M1);
            M1.body.collideWorldBounds = true;
            M1.anchor.setTo(0.5, 0.5)
            M1.body.velocity.x = Math.random() < 0.5 ? 400 : -400
            M1.body.velocity.y = 200
            /*             M1.body.velocity.y = -120; */
            M1.body.bounce.x = 1;
            M1.body.setCircle(M1.height / 2 - 4);
        })
        this.monsterOne.setAll('checkWorldBounds', true);
        this.monsterOne.setAll('health', 200 + game.global.level * 20)

        this.monsterTwo = game.add.group()
        game.add.sprite(0, 1200, 'MonsterTwo', 0, this.monsterTwo)
        game.add.sprite(120, 1160, 'MonsterTwo', 0, this.monsterTwo)
        game.add.sprite(240, 1120, 'MonsterTwo', 0, this.monsterTwo)
        game.add.sprite(360, 1080, 'MonsterTwo', 0, this.monsterTwo)
        game.add.sprite(480, 1040, 'MonsterTwo', 0, this.monsterTwo)

        this.monsterTwo.enableBody = true;
        this.monsterTwo.physicsBodyType = Phaser.Physics.ARCADE;
        this.monsterTwo.children.forEach(function (M2) {
            game.physics.arcade.enable(M2);
            M2.body.collideWorldBounds = true;
            M2.anchor.setTo(0.5, 0.5)
            /*  M2.body.velocity.y = -120; */
            M2.body.velocity.x = 60
            M2.body.bounce.x = 1;
            M2.animations.add('move', [0, , 1, 2, 3], 4, true);
        })
        this.monsterTwo.setAll('checkWorldBounds', true);
        this.monsterTwo.setAll('health', 10 + game.global.level * 20)

        if (game.global.level > 1) {
            this.GameBoss = game.add.sprite(450, 100, 'bossNormal')
            console.log("Y:", this.GameBoss.y)
            this.GameBoss.enableBody = true
            this.GameBoss.anchor.setTo(0.5, 0.5);
            this.GameBoss.animations.add('bossMove', [0, 1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13, 14, 15, 16, 17, 18, 19, 20, 21, 22, 23, 24, 25, 26, 27, 28], 16, true)
            this.GameBoss.physicsBodyType = Phaser.Physics.ARCADE;
            game.physics.arcade.enable(this.GameBoss);
            /* this.GameBoss.body.collideWorldBounds = true; */
            /* this.GameBoss.body.velocity.y = -120 */
            /* this.GameBoss.checkWorldBounds = true */
            this.GameBoss.health = 400 + +game.global.level * 20
            this.GameBoss.name = "BOSS"

            this.BossBul = game.add.group()
            this.BossBul.enableBody = true;
            this.BossBul.createMultiple(50, 'BossBul1');
            this.BossBul.setAll('anchor.x', 0.5);
            this.BossBul.setAll('anchor.y', 0.5);
            this.BossBul.setAll('outOfBoundsKill', true);
            this.BossBul.setAll('checkWorldBounds', true);
            this.BossBul.setAll('name', "bossbul1");

            this.BossBulR = game.add.group()
            this.BossBulR.enableBody = true;
            this.BossBulR.createMultiple(50, 'BossBul1');
            this.BossBulR.setAll('anchor.x', 0.5);
            this.BossBulR.setAll('anchor.y', 0.5);
            this.BossBulR.setAll('outOfBoundsKill', true);
            this.BossBulR.setAll('checkWorldBounds', true);
            this.BossBulR.setAll('name', "bossbulR1");
        }



    },
    playerHit: function (player, enemy) {
        var a = this.hearts.getFirstExists()
        if (a && !Invincible) {
            a.kill()
            Invincible = true
            game.time.events.add(Phaser.Timer.SECOND * 4, this.DisarmInvincible, this);
            game.time.events.repeat(Phaser.Timer.SECOND * 0.2, 20, () => {
                if (this.player.alpha == 1) this.player.alpha = 0.2
                else this.player.alpha = 1
            }, this);
            if (this.hearts.countLiving() == 0) game.state.start('over')
        }
    },
    enemyHit: function (b, e) {
        /* console.log(this.monsterTwo.total) */
        if (b.name == 'lazerH' || b.name == 'lazerT') {
            e.health -= 3;
        } else {
            if (b.name == "BOSS") {
                if (e.name == "b1") {
                    this.emitter.x = e.x;
                    this.emitter.y = e.y;
                    this.emitter.start(false, 100, 1, 15);

                } else if (e.name == "b2") {
                    this.emitter2.x = e.x;
                    this.emitter2.y = e.y;
                    this.emitter2.start(false, 100, 1, 15);

                } else if (e.name == "b3") {
                    this.emitter3.x = e.x;
                    this.emitter3.y = e.y;
                    this.emitter3.start(false, 100, 1, 15);
                }
                b.health -= 1
                if (e.name != 'blade')
                    e.kill()
                if (b.health <= 0) {
                    this.Mdie.play()
                    this.bloodexp(b.x, b.y)
                    b.kill()
                }
            } else {
                if (b.name == "b1") {
                    this.emitter.x = b.x;
                    this.emitter.y = b.y;
                    this.emitter.start(false, 100, 1, 15);

                } else if (b.name == "b2") {
                    this.emitter2.x = b.x;
                    this.emitter2.y = b.y;
                    this.emitter2.start(false, 100, 1, 15);

                } else if (b.name == "b3") {
                    this.emitter3.x = b.x;
                    this.emitter3.y = b.y;
                    this.emitter3.start(false, 100, 1, 15);

                }
                e.health -= 1;
                if (b.name != 'blade')
                    b.kill()
                if (e.health <= 0) {
                    monsterDead++
                    this.Mdie.play()
                    this.bloodexp(e.x, e.y)
                    e.kill()
                }
            }
            if (skillEnergy < 240)
                skillEnergy += 1
            /* console.log(b.name, e.name)

            console.log(this.GameBoss.health) */
        }
    },
    bulletKill: function () {
        var playery = this.player.y
        this.bullets.children.forEach(function (b) {
            if (b.y <= game.camera.y) {
                b.kill()
            }
        })
        this.bulletsLeft.children.forEach(function (b) {
            if (b.y <= game.camera.y) {
                b.kill()
            }
        })
        this.bulletsRight.children.forEach(function (b) {
            if (b.y <= game.camera.y) {
                b.kill()
            }
        })
    },
    DisarmInvincible: function () {
        Invincible = false
        this.player.alpha = 1
    },
    pauseMenu: function (e) {
        if (game.paused) {
            if (e.x >= this.resumeButton.getBounds().x && e.x <= this.resumeButton.getBounds().x + this.resumeButton.width &&
                e.y - game.world.y >= this.resumeButton.y && e.y - game.world.y <= this.resumeButton.y + this.resumeButton.height) {
                if (this.resumeButton) this.resumeButton.destroy()
                if (this.restartButton) this.restartButton.destroy()
                if (this.quittButton) this.quittButton.destroy()
                if (this.soundBar) this.soundBar.destroy()
                if (this.soundMB) this.soundMB.destroy()
                if (this.soundPB) this.soundPB.destroy()
                if (this.volumeText) this.volumeText.destroy()
                game.paused = false
            } else if (e.x >= this.restartButton.getBounds().x && e.x <= this.restartButton.getBounds().x + this.restartButton.width &&
                e.y - game.world.y >= this.restartButton.y && e.y - game.world.y <= this.restartButton.y + this.restartButton.height) {
                game.paused = false
                game.state.start('play')
            } else if (e.x >= this.quittButton.getBounds().x && e.x <= this.quittButton.getBounds().x + this.quittButton.width &&
                e.y - game.world.y >= this.quittButton.y && e.y - game.world.y <= this.quittButton.y + this.quittButton.height) {
                game.paused = false
                game.state.start('start')
            } else if (e.x >= this.soundPB.getBounds().x && e.x <= this.soundPB.getBounds().x + this.soundPB.width &&
                e.y - game.world.y >= this.soundPB.y && e.y - game.world.y <= this.soundPB.y + this.soundPB.height) {
                game.global.musicVolume += 0.1
                this.volumeText.text = parseFloat(game.global.musicVolume + 0.1).toFixed(1)
            } else if (e.x >= this.soundMB.getBounds().x && e.x <= this.soundMB.getBounds().x + this.soundMB.width &&
                e.y - game.world.y >= this.soundMB.y && e.y - game.world.y <= this.soundMB.y + this.soundMB.height) {
                game.global.musicVolume -= 0.1
                this.volumeText.text = parseFloat(game.global.musicVolume - 0.1).toFixed(1)
            }
        }
    },
    LazerSkill: function () {
        if (skillEnergy <= 240)
            this.energyBarOn.width = skillEnergy
        if (this.spaceKey.isDown && skillEnergy == 240) Lazering = true
        if (Lazering) {
            if (!this.lazerH || !this.lazerT) {
                if (!this.lazerH) {
                    this.lazerH = game.add.sprite(this.player.x, this.player.y - 137, 'lazerH')
                    this.lazerH.anchor.setTo(0.5, 0)
                    this.lazerH.name = 'lazerH'
                    this.lazerH.enableBody = true
                    game.physics.arcade.enable(this.lazerH);
                }
                if (!this.lazerT) {
                    this.lazerT = game.add.tileSprite(0, 0, 54, 394, 'lazerT')
                    this.lazerT.anchor.setTo(0.5, 1)
                    this.lazerT.name = 'lazerT'
                    this.lazerT.enableBody = true
                    game.physics.arcade.enable(this.lazerT);
                }
            } else {
                if (this.lazerH) {
                    this.lazerH.x = this.player.x
                    this.lazerH.y = this.player.y - 137
                    game.physics.arcade.overlap(this.lazerH, this.monsterOne, this.enemyHit, null, this)
                    game.physics.arcade.overlap(this.lazerH, this.monsterTwo, this.enemyHit, null, this)
                    game.physics.arcade.overlap(this.lazerH, this.GameBoss, this.enemyHit, null, this)

                }
                if (this.lazerT) {
                    this.lazerT.x = this.player.x - 1
                    this.lazerT.y = this.player.y - 137
                    if (this.lazerT.tilePosition)
                        this.lazerT.tilePosition.y -= 10
                    game.physics.arcade.overlap(this.lazerT, this.monsterOne, this.enemyHit, null, this)
                    game.physics.arcade.overlap(this.lazerT, this.monsterTwo, this.enemyHit, null, this)
                    game.physics.arcade.overlap(this.lazerT, this.GameBoss, this.enemyHit, null, this)
                }
            }
            skillEnergy -= 2
            if (skillEnergy <= 0) Lazering = false
        } else {
            if (this.lazerH) {
                this.lazerH.destroy()
                this.lazerH = null
            }
            if (this.lazerT) {
                this.lazerT.destroy()
                this.lazerT = null
            }
        }
    },
    moveMonster: function () {

        var M1move = false,
            M2move = false
        if (this.monsterOne) {
            this.monsterOne.children.forEach(function (M1) {
                if (game.camera.view.intersectsRaw(M1.left, M1.right, M1.top, M1.bottom)) {

                } else {

                }
            }, this)
        }
        if (this.monsterTwo) {
            this.monsterTwo.children.forEach(function (M2) {
                if (game.camera.view.intersectsRaw(M2.left, M2.right, M2.top - 120, M2.bottom - 120)) {
                    M2.body.velocity.y = -120
                    if (!M2.Shooting) {
                        M2.Shooting = true
                        game.time.events.loop(1000, () => {
                            this.MonsterFire(M2)
                        }, this)
                    }
                } else {
                    /*  game.time.loop(Phaser.Timer.SECOND, () => {
                     
                     }, this) */
                }
            }, this)
        }
        if (this.GameBoss) {
            if (game.camera.view.intersectsRaw(this.GameBoss.left, this.GameBoss.right, this.GameBoss.top - 120, this.GameBoss.bottom - 120)) {
                this.bossAttack = true
            } else {
                this.bossAttack = false
            }
        }
    },
    MonsterFire: function (Monster) {
        if (Monster.alive) {
            var bullet = this.M2Bullets.getFirstExists(false)
            if (bullet) {
                bullet.reset(Monster.x, Monster.y - 8);
                bullet.body.velocity.y = 650;
            }
        }
    },
    getItem: function (p, item) {
        if (item.name == 'item1') {

        } else if (item.name == 'item2') {

        } else if (item.name == 'item3') {
            this.helper = game.add.sprite(this.player.x + 50, this.player.y, 'helper')
            this.helper.anchor.setTo(0.5, 0)
        }
        item.kill()
    },
    explosion: function (b, m) {
        if (this.explode) {
            this.explode.x = b.x
            this.explode.y = b.y
            this.explode.play('explode', 30, false)
        }
        if (b.name == 'expbullet') {
            b.kill()
            m.health -= 20
            if (m.health <= 0) m.kill()
        }

        if (m.name == 'expbullet') {
            m.kill()
            b.health -= 20
            if (b.health <= 0) b.kill()
        }

    },
    bloodexp: function (x, y) {
        this.expEmitter.x = x
        this.expEmitter.y = y
        this.expEmitter.start(true, 800, null, 200);
    },
    defend: function (s, b) {
        if (skillEnergy >= 10) {
            skillEnergy -= 10
            b.kill()
        }
    }
}