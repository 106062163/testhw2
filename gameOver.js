var nameField
var pName = ''
var deleteKey
var pressed = false
var OverState = {
    preload: function () {

    },
    create: function () {
        var OverString = game.add.text(game.world.width / 2, 138, 'Game Over!', {
            font: '64px Arial',
            fill: '#fff'
        })
        var Score = game.add.text(game.world.width / 2, 208, 'Your score: ' + game.global.score, {
            font: '34px Arial',
            fill: '#fff'
        })

        nameField = game.add.text(game.world.width / 2, 258, 'Name:', {
            font: '34px Arial',
            fill: '#fff'
        })

        OverString.anchor.setTo(0.5, 0.5)
        Score.anchor.setTo(0.5, 0.5)
        nameField.anchor.setTo(0.5, 0.5)
        this.restartButton = game.add.sprite(game.world.width / 2 - 135, 400, 'restart')
        this.quittButton = game.add.sprite(game.world.width / 2 - 135, 480, 'quit')
        this.rankButton = game.add.sprite(game.world.width / 2 - 135, 310, 'rank')
        game.input.keyboard.addCallbacks(this, null, null, this.keyDown);
        deleteKey = game.input.keyboard.addKey(Phaser.Keyboard.BACKSPACE);

       /*  game.state.start('rank') */
    },
    update: function () {
        game.input.onDown.add(this.touchButton, this);
        if (deleteKey.isDown && !pressed) {
            pressed = true
            if (pName != '') {
                pName = pName.substring(0, pName.length - 1)
                nameField.text = 'Name:' + pName
            }
        }
        if (!deleteKey.isDown) pressed = false
    },
    touchButton: function (e) {
        if (e.x >= this.restartButton.getBounds().x && e.x <= this.restartButton.getBounds().x + this.restartButton.width &&
            e.y - game.world.y >= this.restartButton.y && e.y - game.world.y <= this.restartButton.y + this.restartButton.height) {
            this.SaveInfor('play')
        } else if (e.x >= this.quittButton.getBounds().x && e.x <= this.quittButton.getBounds().x + this.quittButton.width &&
            e.y - game.world.y >= this.quittButton.y && e.y - game.world.y <= this.quittButton.y + this.quittButton.height) {
            this.SaveInfor('start')
        } else if (e.x >= this.rankButton.getBounds().x && e.x <= this.rankButton.getBounds().x + this.rankButton.width &&
            e.y - game.world.y >= this.rankButton.y && e.y - game.world.y <= this.rankButton.y + this.rankButton.height) {
            this.SaveInfor('rank')
        }
    },
    keyDown: function (word) {
        pName += word
        nameField.text = 'Name:' + pName
    },
    SaveInfor: function (nextState) {
        if (pName == '') {
            alert('input your name for saveing your ranking')
            return;
        };
        var s = game.global.score + ''
        var updateData = {
            Name: pName,
            Score: s.padStart(9, "0")
        }
        firebase.database().ref("/Ranking/").push(updateData).then(function () {
            pName = ''
            if (nextState == 'play') game.state.start('play')
            else if (nextState == 'start') game.state.start('start')
            else if (nextState == 'rank') game.state.start('rank')
        }).catch((e) => {
            console.log(e.message)
        })
    }
}