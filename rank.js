var page = 1
var maxpage
var pageLabel
var allPlayer = new Array()
var rankForm = new Array()
class playerClass {
    constructor(name, score) {
        this.name = name
        this.score = score
    }
    name() {
        return this.name
    }
    score() {
        return this.score
    }
}
var RankState = {
    preload: function () {

    },
    create: function () {
        var rankeLabel = game.add.text(game.world.centerX, 50,
            'Ranking', {
                font: '70px Geo',
                fill: '#ffffff'
            });
        rankeLabel.anchor.setTo(0.5, 0.5)
        var rankRef = firebase.database().ref('Ranking/')

        rankRef.orderByChild("Score").once('value', (data) => {
            var num = 0
            data.forEach(element => {
                num++
                allPlayer[num] = new playerClass(element.val().Name, parseInt(element.val().Score, 10))
            });
            maxpage = Math.round(num / 8)
        }).then(() => {
            var i;
            for (i = 1; i < 9; i++) {
                rankForm[i] = game.add.text(100, 20 + i * 60,
                    '', {
                        font: '70px Geo',
                        fill: '#ffffff'
                    });
                if (allPlayer[8 * (page - 1) + i])
                    rankForm[i].text = 8 * (page - 1) + i + '.' + allPlayer[8 * (page - 1) + i].name + '----' + allPlayer[8 * (page - 1) + i].score + 'points'
                else
                    rankForm[i].text = ''
                /* rankForm[i].anchor.setTo(1, 0) */
            }
        })

        pageLabel = game.add.text(game.world.width - 200, 550,
            'LEFT <' + page + '> RIGHT', {
                font: '30px Geo',
                fill: '#ffffff'
            });
        this.reBut = game.add.button(750, 20, 'reIcon', () => {
            page = 1
            maxpage = 1
            game.state.start('start')
        },this)
    },
    update: function () {
        game.input.keyboard.onUpCallback = (e) => {
            if (e.keyCode == Phaser.Keyboard.LEFT) {
                if (page > 1) {
                    page--
                    this.updateRank()
                }

            } else if (e.keyCode == Phaser.Keyboard.RIGHT) {
                if (page < maxpage) {
                    page++
                    this.updateRank()
                }
            }
        };

    },
    updateRank: function () {
        for (var i = 1; i < 9; i++) {
            if (allPlayer[8 * (page - 1) + i])
                rankForm[i].text = 8 * (page - 1) + i + '.' + allPlayer[8 * (page - 1) + i].name + '----' + allPlayer[8 * (page - 1) + i].score + 'points'
            else
                rankForm[i].text = ''
        }
    }

}