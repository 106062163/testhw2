var LoadState = {
    preload: function () {
        var loadingLabel = game.add.text(game.width / 2, 150,
            'loading...', {
                font: '30px Arial',
                fill: '#ffffff'
            });
        loadingLabel.anchor.setTo(0.5, 0.5);
        this.progressBar = game.add.sprite(game.width / 2, 200, 'progressBar');
        this.progressBar.anchor.setTo(0.5, 0.5);
        game.load.setPreloadSprite(this.progressBar);
        game.load.spritesheet('player', '/image/player64.png', 74, 74);
        game.load.spritesheet('MonsterTwo', '/image/M2_sheet.png', 55, 150)
        game.load.spritesheet('bossNormal', '/image/boss_normal.png', 400, 289)
        game.load.spritesheet('bossAttack', '/image/boss_attack.png', 400, 400)
        game.load.spritesheet('explode', '/image/spritesheet2-300x300.png', 120, 120)

        game.load.image('helper', '/image/helper.png')
        game.load.image('bullet', '/image/bullet.PNG')
        game.load.image('heart', '/image/heart.png')
        game.load.image('background', '/image/background.jpg')
        game.load.image('MonsterOne', '/image/spacestation.png')
        game.load.image('lazer', '/image/lazer1_1.png')
        game.load.image('pixel', 'image/pixel.png');
        game.load.image('blood', 'image/blood.png');
        game.load.image('pause', '/image/pause.png')
        game.load.image('resume', '/image/resume.PNG')
        game.load.image('restart', '/image/restart.PNG')
        game.load.image('quit', '/image/Quit.PNG')
        game.load.image('lazerH', './image/lazerHead.png')
        game.load.image('lazerT', './image/lazerTail.png')
        game.load.image('energyBar', '/image/EnergyBar.png')
        game.load.image('barOn', './image/EnergyBarOn.png')
        game.load.image('M2_bullet', '/image/M2_bullet.PNG')
        game.load.audio('bgMusic', '/image/Puzzle-Game.mp3')
        game.load.audio('Mdie', '/image/Mdie.mp3')
        game.load.image('soundBar', '/image/soundBar.png')
        game.load.image('soundPlus', '/image/soundPlus.png')
        game.load.image('soundMinus', '/image/soundMinus.png')
        game.load.image('rank', './image/rank.PNG')
        game.load.image('reIcon', './image/restartIcon.png')
        game.load.image('BossBul1', '/image/BossBullet1.PNG')
        game.load.image('item1', '/image/item1.png')
        game.load.image('item2', '/image/item2.png')
        game.load.image('item3', '/image/item3.png')
        game.load.image('expbullet', '/image/expBullet.png')
        game.load.image('helpB', './image/helperBullet.png')
        game.load.image('p2', './image/player2.png')
        game.load.image('shield', './image/magic_007.png')
        game.load.spritesheet('blade','/image/p2Blade.png',60,134)
        
    },
    create: function () {
        game.state.start('play')
    },
    update: function () {

    }
}