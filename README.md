# Software Studio 2019 Spring Assignment 2
## Notice
* Replace all [xxxx] to your answer

## Basic Components
|Component|Score|Y/N|
|:-:|:-:|:-:|
|Complete game process|15%|Y|
|Basic rules|20%|Y|
|Jucify mechanisms|15%|Y|
|Animations|10%|Y|
|Particle Systems|10%|Y|
|UI|5%|Y|
|Sound effects|5%|Y|
|Leaderboard|5%|Y|

## Website Detail Description

# Basic Components Description : 
1. Jucify mechanisms :
* <h4> Except level 1, player need to kill the boss then go to next level</h4>
* <h4> In level 1, just have 5 monster need to kill</h4>
* <h4>Level display in top of window :  <img src="/report/level.PNG" alt=""></h4>
* <h4>For using skill, need skill energy, get energy by attacking monster</h4>
* <h4>when skill energy is full, then can press Space to use it
<img src="/report/skill.gif" alt="" style="display:block"></h4>
<br>
2. Animations :
* <h4> Player move left and right with animation : <img src="/report/player.gif" alt=""></h4>
3. Particle Systems : [xxxx]
4. Sound effects : [xxxx]
5. Leaderboard : [xxxx]

# Bonus Functions Description : 
1. [xxxx] : [xxxx]
2. [xxxx] : [xxxx]
3. [xxxx] : [xxxx]
